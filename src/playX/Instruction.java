package playX;

public class Instruction {
    public static final int PUSHI = -1;
    public static final int PUSHF = -2;
    public static final int PUSHS = -3;
    public static final int IADD = 2;
    public static final int ISUB = 3;
    public static final int FSUB = -4;
    public static final int FADD = -5;
    public static final int IRET = 0xf28;
    public static final int FRET = 0xf29;
    public static final int VRET = 0xf30;
    public static final int JMP = 4;
    public static final int IEQ = 5;
    public static final int IGT = 6;
    public static final int ILT = 7;
    public static final int FEQ = 0x4f;
    public static final int FGT = 0x5f;
    public static final int FLT = 0x6f;
    public static final int BRET = 0x7f;
}
