package playX;

public class Value {
    private Object value;

    private String type;
    public String[] types = {
            "Int",
            "Float",
            "Str",
            "Boolean",

    };

    public Value(Object v,String t) {
        this.value = v;
        type = t;
    }

    public String getType() {
        return type;
    }

    public Object getValue() {
        return value;
    }

    @Override
    public String toString() {
        return this.value.toString();
    }
}
