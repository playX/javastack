package playX;

import java.util.Stack;

public class Interpreter {

    public Stack<Value> stack = new Stack<Value>();

    public int pc = 0;
    public Object ret = null;

    public Object[] code = {};

    public Interpreter(Object[] code) {
        this.code = code;
    }



    public void interpret() {
        while (pc < code.length) {
            if (ret == null) {
                switch ((int) code[pc]) {
                    case Instruction.PUSHI:
                        pc++;
                        Value val = new Value((int) code[pc], "Int");
                        stack.push(val);
                        pc++;
                        continue;

                    case Instruction.PUSHF:
                        pc++;
                        val = new Value((double) code[pc], "Float");
                        stack.push(val);
                        pc++;
                        continue;
                    case Instruction.IADD:
                        int value = (int) stack.pop().getValue();
                        int value2 = (int) stack.pop().getValue();
                        stack.push(new Value(value + value2, "Int"));
                        pc++;
                        continue;
                    case Instruction.FADD:
                        double dvalue = (double) stack.pop().getValue();
                        double dvalue2 = (double) stack.pop().getValue();
                        stack.push(new Value(dvalue + dvalue2, "Double"));
                        pc++;
                        continue;
                    case Instruction.JMP:
                        pc++;
                        int idx = (int) code[pc];
                        pc = idx;
                        continue;

                    case Instruction.IRET:
                        ret = (int) stack.pop().getValue();
                        pc++;
                        continue;
                    case Instruction.FRET:
                        ret = (double) stack.pop().getValue();
                        pc++;
                        continue;
                    case Instruction.FGT:
                        dvalue = (double) stack.pop().getValue();
                        dvalue2 = (double) stack.pop().getValue();

                        pc++;
                        stack.push(new Value(dvalue > dvalue2, "Boolean"));
                        continue;
                    case Instruction.FEQ:
                        dvalue = (double) stack.pop().getValue();
                        dvalue2 = (double) stack.pop().getValue();

                        pc++;
                        stack.push(new Value(dvalue == dvalue2, "Boolean"));
                        continue;
                    case Instruction.FLT:
                        dvalue = (double) stack.pop().getValue();
                        dvalue2 = (double) stack.pop().getValue();

                        pc++;
                        stack.push(new Value(dvalue < dvalue2, "Boolean"));
                        continue;
                    case Instruction.IEQ:
                        value = (int) stack.pop().getValue();
                        value2 = (int) stack.pop().getValue();

                        pc++;
                        stack.push(new Value(value > value2, "Boolean"));
                        continue;

                    case Instruction.BRET:
                        ret = (boolean) stack.pop().getValue();
                }
                pc++;

            } else {
                break;
            }
        }
    }


}
